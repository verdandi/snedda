# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# = require jquery
# = require jquery_ujs
# = require bootstrap/collapse
# = require turbolinks

ready = ->
	$toggle = $('.navbar-toggle')
	$html = $('html,body')
	$register = $('#register')
	$login = $('#login')
	$overlay = $('.overlay')
	$('a[href=#register]').on 'click', (e) ->
		e.preventDefault()

		# hide and animate
		unless $register.hasClass('active')
			$login.removeClass('active')
			$html.animate({ scrollTop: 0 }, 'slow')

		$register.toggleClass('active') # toggle to make it possible to click link again
		$toggle.trigger('click')
	$('a[href=#login]').on 'click', (e) ->
		e.preventDefault()
		unless $login.hasClass('active')
			$register.removeClass('active')
			$html.animate({ scrollTop: 0 }, 'slow')
		$login.toggleClass('active')
		$toggle.trigger('click')
	$('a.close').on 'click', (e) ->
		e.preventDefault()
		$overlay.removeClass('active')
$(document).ready ready
$(document).on 'page:load', ready

