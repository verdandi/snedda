# = require jquery
# = require jquery_ujs
#  require jquery.turbolinks
# = require jquery.touchswipe
# = require jquery.tooltipster
#  require bootstrap
# = require bootstrap/dropdown
# = require bootstrap/affix
# = require turbolinks
# = require_directory .
ready = ->
	# $("span.has-tip").popover
	# 	trigger: 'click'
	# 	placement: 'top'
	$('span.has-tip').tooltipster()

	$button = $('#menu')
	$content = $('.slide')
	$menu = $('.sidebar')
	showMenu =  ->
		return if not $button.is(':visible') #Quick check to see if we can slide to the left (media query disables this)
		$button.data('open',true)
		$button.addClass('active')
		# $menu.addClass('active')
		$content.addClass('slideLeft')
	hideMenu =  ->
		return if not $button.is(':visible') #Quick check to see if we can slide to the left (media query disables this)
		$button.data('open',false)
		$button.removeClass('active')
		# $menu.removeClass('active')
		$content.removeClass('slideLeft')
	$button.on 'touchstart click', (e) ->
		e.stopPropagation()
		e.preventDefault()
		$this = $(this)
		if $this.data('open') isnt true
			showMenu()
		else
			hideMenu()
	$('body').swipe
		# swipe: (event, direction, distance, duration, fingerCount) ->
		# 	console.log "Swipe #{direction}"
		# 	if direction is 'right'
		# 		showMenu()
		# 	else if direction is 'left'
		# 		hideMenu()
		# allowPageScroll:"vertical"
		swipeRight: (event, direction, distance, duration, fingerCount) ->
			showMenu()
		swipeLeft: (event, direction, distance, duration, fingerCount) ->
			hideMenu()
		threshold: 100
	scrollTo = ($el, c)->
		setTimeout ->
			$("body").animate
				scrollTop: $el.offset().top - 80 # the body padding plus something additional
			, 1000, c
		,500
	if window.location.hash.indexOf("#heimsmynd") is 0
		scrollTo $("#{window.location.hash}x")

	$('a.print').on 'click', (e) ->
		e.preventDefault()
		window.print()
$(document).ready ready
$(document).on 'page:load', ready
	
