ready = ->
	play = $('.play')
	stop = $('.stop')
	pause = $('.pause')
	slider = $('.seek input')
	volume = $('.volume')
	volume_button = volume.find('.fa')
	controls = volume.find('.controls')
	volume_slider = controls.find('input')
	chapter = new Audio()
	duration = chapter.duration
    
	chapter.addEventListener 'error', ->
    	console.log "An error occured with code #{chapter.error.code}"
    	console.debug chapter.error



	$('a#openPlayer').on 'click', (e) ->
		e.preventDefault()
		$(this).toggleClass('active')
		$('.player').slideToggle(400)
		controls.slideUp(200)
		return if chapter.src isnt ''
		if chapter.canPlayType('audio/mpeg')
		    chapter.type = 'audio/mpeg'
		    chapter.src = "#{window.audio}.mp3"
	    else
		    chapter.type = 'audio/ogg'
		    chapter.src = "#{window.audio}.ogg"

	play.on 'touchstart click', (e) ->
		e.stopPropagation()
		e.preventDefault()

		# console.log 'play'
		$(this).hide()
		$('.pause').show()
		chapter.play()
		chapter.volume = volume_slider.val() / 10.0
		slider.attr('max', chapter.duration)

	stop.on 'touchstart click', (e) ->
		e.stopPropagation()
		e.preventDefault()

		# console.log 'stop'
		chapter.pause()
		chapter.currentTime = 0

	pause.on 'touchstart click', (e) ->
		e.stopPropagation()
		e.preventDefault()

		# console.log 'pause'
		$(this).hide()
		$('.play').show()
		chapter.pause()

	volume_button.on 'touchstart click', (e) ->
		e.stopPropagation()
		e.preventDefault()

		e.handled = true
		console.log 'volume'
		controls.slideToggle(200)


	slider.on 'change', ->
		chapter.currentTime = $(this).val()
		slider.attr("max", chapter.duration);

	chapter.addEventListener 'timeupdate', ->
		curtime = parseInt(chapter.currentTime, 10)
		slider.val(curtime)

	volume_slider.on 'change', ->
		chapter.volume = $(this).val() / 10.0


$(document).ready ready
$(document).on 'page:load', ready