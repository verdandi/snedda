module Chapters::ChapterHelper
	def current_page(page, action = "chapters")
	  if controller.controller_name.to_s == page and controller.action_name.to_s == action
	    'active'
	  else
	    'not_active'
	  end
	end
	def current_group(group)
		g = controller.controller_name.to_s.gsub(/[^\d]/, '').to_i
		if controller.controller_name.to_s == 'map' then g = 1 end
		if controller.controller_name.to_s == 'exercises' then g = 15 end
		ge = 0
		if group == 1 then ge = 1..8 end
		if group == 2 then ge = 9..14 end
		if group == 3 then ge = 15..16 end
		if ge.include? g 
			'active'
		else
			'not_active'
		end
	end
	@@chapters = nil

	def next_page
		@@chapters ||= [
			skopunheimsins_path,
			heimsmyndin_path,
			aesirogasynjur_path,
			valholl_path,
			sleipnirogskidbladnir_path,
			forthorstilutgardaloka_path,
			daudibaldurs_path,
			ragnarok_path,
			thjassiogidunn_path,
			skaldamjodurinn_path,
			forthors_path,
			haddur_path,
			fafnisarfur_path
		]
		path = request.fullpath == '/' ? skopunheimsins_path : request.fullpath
		index = @@chapters.index path
		if index.nil? or @@chapters[index + 1].nil?
			return ''
		else 
			return @@chapters[index + 1]
		end
	end

	def previous_page
		@@chapters ||= [
			skopunheimsins_path,
			heimsmyndin_path,
			aesirogasynjur_path,
			valholl_path,
			sleipnirogskidbladnir_path,
			forthorstilutgardaloka_path,
			daudibaldurs_path,
			ragnarok_path,
			thjassiogidunn_path,
			skaldamjodurinn_path,
			forthors_path,
			haddur_path,
			fafnisarfur_path
		]
		path = request.fullpath == '/' ? skopunheimsins_path : request.fullpath
		index = @@chapters.index path

		if index.nil? or index == 0 or @@chapters[index - 1].nil?
			return ''
		else 
			return @@chapters[index - 1]
		end
	end
end