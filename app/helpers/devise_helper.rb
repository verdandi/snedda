module DeviseHelper
  # A simple way to show error messages for the current devise resource. If you need
  # to customize this method, you can either overwrite it in your application helpers or
  # copy the views to your application.
  #
  # This method is intended to stay simple and it is unlikely that we are going to change
  # it to add more behavior or options.
  def devise_error_messages!
    puts flash.inspect
    html = ""
    if flash[:success]

      html += <<-HTML
      <div class="alert alert-success">
        <p>#{flash[:success]}</p>
      </div>
      HTML
    end
    if flash[:notice]
      html += <<-HTML
      <div class="alert alert-warning">
        <p>#{flash[:notice]}</p>
      </div>
      HTML
    end
    flash_alerts = []
    error_key = 'errors.messages.not_saved'

    if !flash.empty?
      flash_alerts.push(flash[:error]) if flash[:error]
      flash_alerts.push(flash[:alert]) if flash[:alert]
      # if session['devise.multiple_error']
      #   flash_alerts.push(session['devise.multiple_error'])
      #   session.delete 'devise.multiple_error'
      # end
      error_key = 'devise.failure.error'
    end

    # session['devise.multiple_error'] = flash[:alert][5..-1] if flash[:alert] and flash[:alert].start_with? "2398 " #HOTFIX FTW (fixplz)
    return html.html_safe if resource.errors.empty? && flash_alerts.empty?
    errors = resource.errors.empty? ? flash_alerts : resource.errors.full_messages


    html += errors.map { |msg| content_tag(:div, content_tag(:p,msg), class: "alert alert-danger") }.join
    # sentence = I18n.t(error_key, :count    => errors.count,
    #                              :resource => resource.class.model_name.human.downcase)

    # html += <<-HTML
    # <div class="alert alert-danger">
    #   <p>#{sentence}</p><br />
    #   <ul>#{messages}</ul>
    # </div>
    # HTML

    

    html.html_safe
  end
end