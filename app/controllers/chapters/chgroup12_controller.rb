# encoding: utf-8

class Chapters::Chgroup12Controller < Chapters::ChapterController
  def chapters    
    @chapter = "Haddur Sifjar"
    @chapters = ""
    @audio = "audio/Haddur"
    @image = "chapters/38-41.jpg"
    # @no_toolbar = true
  end
end
