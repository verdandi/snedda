# encoding: utf-8

class Chapters::Chgroup8Controller < Chapters::ChapterController
	def chapters
	    @chapter = "Ragnarök"
	    @chapters = "kaflar 51 - 54"
	    @audio = "audio/G8"
	    @image = "chapters/51-54.jpg"
	end
end
