# encoding: utf-8

class Chapters::Chgroup5Controller < Chapters::ChapterController
	def chapters
		@chapter = "Sleipnir og Skíðblaðnir"
		@chapters = "kaflar 42 - 43"

	    @audio = "audio/G5"
	    @image = "chapters/42-43.jpg"
  	end
end
