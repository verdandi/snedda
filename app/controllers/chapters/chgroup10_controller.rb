# encoding: utf-8

class Chapters::Chgroup10Controller < Chapters::ChapterController
  def chapters    
    @chapter = "Skáldamjöðurinn"
    @chapters = ""
    @audio = "audio/Skaldamjodur"
    @image = "chapters/38-41.jpg"
    # @no_toolbar = true
  end
end
