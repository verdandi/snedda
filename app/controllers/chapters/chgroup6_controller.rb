# encoding: utf-8

class Chapters::Chgroup6Controller < Chapters::ChapterController
  def chapters
    @chapter = "För þórs til Útgarða-Loka"
    @chapters = "kaflar 44 - 48"


    @audio = "audio/G6"
    @image = "chapters/44-48.jpg"
  end
end
