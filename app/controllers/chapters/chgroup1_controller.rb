# encoding: utf-8

class Chapters::Chgroup1Controller < Chapters::ChapterController
  def chapters    
    @chapter = "Sköpun heimsins"
    @chapters = "kaflar 1 - 8"
    @audio = "audio/G1"
    @image = "chapters/1-8.jpg"
  end
end
