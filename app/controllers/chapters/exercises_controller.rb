class Chapters::ExercisesController < Chapters::ChapterController
	before_action :disable_chapters!
	def connect
		@id = params[:id]
		unless @id.nil?
			@quiz = ConnectQuiz.includes(:connections).where(:alias => @id).first
			if @quiz.nil?
				redirect_to :back
				return
			end
			@right, @left = @quiz.connections.partition do |a|
				a.location?
			end
			@left.shuffle!
			@right.shuffle!
			render "chapters/exercises/connect/quiz"
		end
	end
	def connect_check
		@id = params[:id]
		@quiz = ConnectQuiz.includes(:connections).where(:alias => @id).first
		if @quiz.nil?
			redirect_to :back
			return
		end
		@answers = params[:answers] || {}
		@count = @quiz.connections.count/2
		@correct = 0
		@connections = @quiz.connections.to_a.index_by(&:id)
		@answers.each do |k,v|
			if @connections[k.to_i].connect_id == v.to_i
				@correct += 1
			end
		end
		render "chapters/exercises/connect/quiz_check"
	end
	def essay
		@id = params[:id]
		unless @id.nil?
			render "chapters/exercises/essay/#{@id}"
		end
	end
	def writing
	end

	def language
	end

	def interpretation 
	end

	def crossword
	end

	def choice
		@id = params[:id]
		unless @id.nil?
			# render "chapters/exercises/choice/#{@id}/choice_#{@chapter}"
			@quiz = Quiz.includes(questions: [:answers]).where(:alias => @id).first
			if @quiz.nil?
				redirect_to :back
				return
			end
			render "chapters/exercises/choice/quiz"
		end
	end

	def choice_check
		@id = params[:id]
		@quiz = Quiz.includes(questions: [:answers]).where(:alias => @id).first
		if @quiz.nil?
			redirect_to :back
			return
		end
		@answers = params[:answers] || {}
		@count = @quiz.questions.count
		@correct = 0
		@questions = @quiz.questions.to_a.index_by(&:id)
		@answers.each do |k,v|
			if @questions[k.to_i].answers.to_a.index_by(&:id)[v.to_i].correct
				@correct += 1
			end
		end
		render "chapters/exercises/choice/quiz_check"
	end

	private
		def disable_chapters!
			# @no_toolbar = true
			@simple_toolbar = true
			@no_image = true
			@print = true
		end
end