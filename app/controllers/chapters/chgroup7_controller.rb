# encoding: utf-8

class Chapters::Chgroup7Controller < Chapters::ChapterController
	def chapters
		@chapter = "Dauði Baldurs"
		@chapters = "kaflar 49 - 50"
	    @audio = "audio/G7"
	    @image = "chapters/49-50.jpg"
	end
end
