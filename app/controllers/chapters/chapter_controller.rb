class Chapters::ChapterController < AuthenticatedController
	layout 'chapters/layouts/application'
	protect_from_forgery with: :exception
end
