class Chapters::Chgroup2Controller < Chapters::ChapterController
  def chapters
    @chapter = "Heimsmyndin"
    @chapters = "kaflar 9 - 19"
    @audio = "audio/G2"
    @image = "chapters/9-19.jpg"
  end
end
