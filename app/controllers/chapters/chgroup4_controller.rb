# encoding: utf-8

class Chapters::Chgroup4Controller < Chapters::ChapterController
  def chapters
    @chapter = "Valhöll"
    @chapters = "kaflar 38 - 41"

    @audio = "audio/G4"
    @image = "chapters/38-41.jpg"
  end
end
