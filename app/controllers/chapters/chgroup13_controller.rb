# encoding: utf-8

class Chapters::Chgroup13Controller < Chapters::ChapterController
  def chapters    
    @chapter = "Fáfnisarfur"
    @chapters = ""
    @audio = "audio/Fafnisarfur"
    @image = "chapters/38-41.jpg"
    # @no_toolbar = true
  end
end
