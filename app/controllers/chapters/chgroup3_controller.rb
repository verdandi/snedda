# encoding: utf-8

class Chapters::Chgroup3Controller < Chapters::ChapterController
  def chapters
    @chapter = "Æsir og ásynjur"
    @chapters = "kaflar 20 - 37"

    @audio = "audio/G3"
    @image = "chapters/20-37.jpg"
  end
end
