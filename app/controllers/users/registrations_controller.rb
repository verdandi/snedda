class Users::RegistrationsController < Devise::RegistrationsController
  protected

  def after_sign_up_path_for(resource)
  	flash[:register] = true
    new_redirect_payment_path(resource.id)
  end
  def after_inactive_sign_up_path_for(resource)
  	flash[:register] = true
    new_redirect_payment_path(resource.id)
  end
end