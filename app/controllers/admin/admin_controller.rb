class Admin::AdminController < AuthenticatedController
	before_action :admin_user!

	layout 'admin/layouts/application'

	private
		def admin_user!
			unless current_user.admin?
				raise ActionController::RoutingError.new('Not Found')
			end
		end
end