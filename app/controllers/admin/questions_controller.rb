class Admin::QuestionsController < Admin::AdminController
	before_filter :find_parent
	def index
		@questions = @quiz.questions
	end
	def create
		@question = @quiz.questions.create question_params
		redirect_to action: "index"
	end
	def new 
		@question = Question.new
		@url = admin_quiz_questions_path(@quiz)
	end

	def edit
		@question = @quiz.questions.find params[:id]
		@url = admin_quiz_question_path(@quiz,@question)
	end

	def update
		@question = @quiz.questions.find params[:id]
		if @question.update_attributes question_params
			redirect_to action: "index"
		end

	end

	def destroy
		@quiz.questions.find(params[:id]).destroy!
		redirect_to action: "index"
	end

	private
		def question_params
			params.require(:question).permit(:question)
		end
		def find_parent
			@quiz = Quiz.find params[:quiz_id]
		end
end