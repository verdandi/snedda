class Admin::QuizzesController < Admin::AdminController
	def index
		@quizzes = Quiz.all
	end
	def create
		@quiz = Quiz.create quiz_params
		redirect_to action: "index"
	end
	def new 
		@url = admin_quizzes_path
		@quiz = Quiz.new
	end

	def edit
		@quiz = Quiz.find params[:id]
		@url = admin_quiz_path(@quiz)
	end

	def update
		@quiz = Quiz.find params[:id]
		if @quiz.update_attributes quiz_params
			redirect_to action: "index"
		end

	end

	def destroy
		Quiz.find(params[:id]).destroy!
		redirect_to action: "index"
	end

	private
		def quiz_params
			params.require(:quiz).permit(:name,:alias,:description)
		end
end