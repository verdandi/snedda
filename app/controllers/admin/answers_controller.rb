class Admin::AnswersController < Admin::AdminController
	before_filter :find_parents
	def index
		@answers = @question.answers
	end
	def create
		@answer = @question.answers.create answer_params
		redirect_to action: "index"
	end
	def new 
		@url = admin_quiz_question_answers_path(@quiz,@question)
		@answer = Answer.new
	end

	def edit
		@answer = @question.answers.find params[:id]
		@url = admin_quiz_question_answer_path(@quiz,@question,@answer)
	end

	def update
		@answer = @question.answers.find params[:id]
		if @answer.update_attributes answer_params
			redirect_to action: "index"
		end

	end

	def destroy
		@question.answers.find(params[:id]).destroy!
		redirect_to action: "index"
	end

	private
		def answer_params
			params.require(:answer).permit(:answer, :correct)
		end
		def find_parents
			@quiz = Quiz.find params[:quiz_id]
			@question = @quiz.questions.find params[:question_id]
		end
end