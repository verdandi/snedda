class Admin::UsersController < Admin::AdminController
	def index
		@users = User.order(:created_at)
		p current_user.admin?
	end

	def admin_enable
		user = User.find params[:id]
		user.update_attribute :admin, true
		redirect_to :back
	end

	def admin_disable
		user = User.find params[:id]
		user.update_attribute :admin, false
		redirect_to :back
	end

	def admin_purchase
		user = User.find params[:id]
		user.update_attribute :purchased, true
		redirect_to :back
	end

	def admin_clear_purchase
		user = User.find params[:id]
		user.update_attribute :purchased, false
		redirect_to :back
	end

	# def admin_logout
	# 	User.all.each do |user|
	# 		sign_out user
	# 	end
	# 	redirect_to :back
	# end

	def delete
		user = User.find params[:id]
		user.destroy!
		redirect_to :back
	end
end
