class Admin::ConnectQuizzesController < Admin::AdminController
	def index
		@quizzes = ConnectQuiz.all
	end
	def create
		@quiz = ConnectQuiz.create quiz_params
		redirect_to action: "index"
	end
	def new 
		@url = admin_connect_quizzes_path
		@quiz = ConnectQuiz.new
	end

	def edit
		@quiz = ConnectQuiz.find params[:id]
		@url = admin_connect_quiz_path(@quiz)
	end

	def update
		@quiz = ConnectQuiz.find params[:id]
		if @quiz.update_attributes quiz_params
			redirect_to action: "index"
		end

	end

	def destroy
		ConnectQuiz.find(params[:id]).destroy!
		redirect_to action: "index"
	end

	private
		def quiz_params
			params.require(:connect_quiz).permit(:name,:alias)
		end
end