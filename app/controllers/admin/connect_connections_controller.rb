class Admin::ConnectConnectionsController < Admin::AdminController
	before_filter :find_parent
	def index
		@connections = @quiz.connections.includes(:connection)
		# @connections = ConnectConnection.all
	end
	def create
		@connection = @quiz.connections.create quiz_params
		if @connection.connect_id
			@connection2 = @quiz.connections.find @connection.connect_id
			if @connection2.update_attributes connect_id: @connection.id
				redirect_to action: "index"
			end
		else
			redirect_to action: "index"
		end
	end
	def new 
		@url = admin_connect_quiz_connect_connections_path(@quiz)
		@connection = ConnectConnection.new
	end

	def edit
		@connection = @quiz.connections.find params[:id]
		@url = admin_connect_quiz_connect_connection_path(@quiz,@connection)
	end

	def update
		@connection = @quiz.connections.find params[:id]
		if @connection.update_attributes quiz_params
			if @connection.connect_id
				@connection2 = @quiz.connections.find @connection.connect_id
				if @connection2.update_attributes connect_id: @connection.id
					redirect_to action: "index"
				end
			end
		end

	end

	def destroy
		@quiz.connections.find(params[:id]).destroy!
		redirect_to action: "index"
	end

	private
		def quiz_params
			params.require(:connect_connection).permit(:answer,:location,:connect_id)
		end
		def find_parent
			@quiz = ConnectQuiz.find params[:connect_quiz_id]
		end
end