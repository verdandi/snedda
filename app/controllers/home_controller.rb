class HomeController < ApplicationController 
	before_filter :logged_in
	layout 'layouts/layout'
	def index
	end

	private
		def logged_in
			if user_signed_in?
				redirect_to "/skopunheimsins"
			end
		end

		def remove_header
			@no_header = true
		end
end
