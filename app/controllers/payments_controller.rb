class PaymentsController < ApplicationController
	def redirect
		if params[:email]
			@user = User.find_by email: params[:email]
		elsif params[:user]
			@user = User.find(params[:user].to_i)
		else
			return redirect_to :back
		end
		if @user.purchased?
			return redirect_to :back
		end
		@payment_url = payment_url
		@payment_params = payment_params @user.id
	end

	def done
		ref = params[:ReferenceNumber]
		hash = Digest::MD5.hexdigest(verification_code + ref)
		if params[:DigitalSignatureResponse] == hash
			flash[:success] = "Greiðsla tókst!"
			redirect_to new_user_session_path
		end
	end

	def purchase
		ref = params[:ReferenceNumber]
		hash = Digest::MD5.hexdigest(verification_code + ref)
		if params[:DigitalSignatureResponse] == hash
			user = User.find(ref.to_i)
			if not user.nil? and user.update_attributes purchased: true, sale_id: params[:SaleID]
				render text: "OK"
				return
			end
		end
		render text: "NO"
	end

	private
		def verification_code
			return "6634hjad2jj8wnj"
		end
		def merchant_id
			return "783"
		end
		def payment_url
			return "https://greidslusida.valitor.is/"
		end
		def payment_params(ref)
			hash = {
				:AuthorizationOnly => "0",
				:Product_1_Quantity => "1",
				:Product_1_Price => "1890",
				:Product_1_Discount => "0",
				:MerchantID => merchant_id,
				:ReferenceNumber => ref,
				:PaymentSuccessfulURL => "http://snorraedda.is/payments/done",
				:PaymentSuccessfulServerSideURL => "http://snorraedda.is/payments/process",
				:Currency => "ISK",
			}
			other_params = {

				:Product_1_Description => "Aðgangur að Snorra Eddu",
				:PaymentSuccessfulURLText => "Snorra Edda.is",
				:stuff => hash.values.join("")
			}
			hash[:DigitalSignature] = Digest::MD5.hexdigest(verification_code + hash.values.join(""))
			return hash.merge other_params
		end
end
