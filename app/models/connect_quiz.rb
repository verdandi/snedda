class ConnectQuiz < ActiveRecord::Base
	# has_many :connect_connections, :dependent => :destroy
	has_many :connections, class_name: "ConnectConnection"
end
