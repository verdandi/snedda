class ConnectConnection < ActiveRecord::Base
  belongs_to :connect_quiz
  has_one :connection, class_name: "ConnectConnection", foreign_key: :connect_id
  belongs_to :connection_back, class_name: "ConnectConnection", foreign_key: :connect_id
end
