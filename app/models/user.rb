class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :session_limitable

  def active_for_authentication? 
    super && purchased? 
  end 

  def inactive_message 
    if !purchased? 
      :not_purchased 
    else 
      super # Use whatever other message 
    end 
  end
end
