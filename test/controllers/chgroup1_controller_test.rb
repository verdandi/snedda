require 'test_helper'

class Chgroup1ControllerTest < ActionController::TestCase
  test "should get ch1" do
    get :ch1
    assert_response :success
  end

  test "should get ch2" do
    get :ch2
    assert_response :success
  end

  test "should get ch3" do
    get :ch3
    assert_response :success
  end

  test "should get ch4" do
    get :ch4
    assert_response :success
  end

  test "should get ch5" do
    get :ch5
    assert_response :success
  end

  test "should get ch6" do
    get :ch6
    assert_response :success
  end

  test "should get ch8" do
    get :ch8
    assert_response :success
  end

end
