require 'test_helper'

class Chgroup2ControllerTest < ActionController::TestCase
  test "should get ch9" do
    get :ch9
    assert_response :success
  end

  test "should get ch10" do
    get :ch10
    assert_response :success
  end

  test "should get ch11" do
    get :ch11
    assert_response :success
  end

  test "should get ch12" do
    get :ch12
    assert_response :success
  end

  test "should get ch13" do
    get :ch13
    assert_response :success
  end

  test "should get ch14" do
    get :ch14
    assert_response :success
  end

  test "should get ch15" do
    get :ch15
    assert_response :success
  end

  test "should get ch16" do
    get :ch16
    assert_response :success
  end

  test "should get ch17" do
    get :ch17
    assert_response :success
  end

  test "should get ch18" do
    get :ch18
    assert_response :success
  end

  test "should get ch19" do
    get :ch19
    assert_response :success
  end

end
