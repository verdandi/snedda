require 'test_helper'

class Chgroup8ControllerTest < ActionController::TestCase
  test "should get ch51" do
    get :ch51
    assert_response :success
  end

  test "should get ch52" do
    get :ch52
    assert_response :success
  end

  test "should get ch53" do
    get :ch53
    assert_response :success
  end

  test "should get ch54" do
    get :ch54
    assert_response :success
  end

end
