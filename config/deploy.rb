require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
# require 'mina/rbenv'  # for rbenv support. (http://rbenv.org)
# require 'mina/rvm'    # for rvm support. (http://rvm.io)
require 'uri'

# Basic settings:
#   domain       - The hostname to SSH to.
#   deploy_to    - Path to deploy into.
#   repository   - Git repo to clone from. (needed by mina/git)
#   branch       - Branch name to deploy. (needed by mina/git)
set :user, 'deploy'
set :domain, '104.155.36.209'
set :deploy_to, '/srv/snedda'
set :repository, 'git@bitbucket.org:verdandi/snedda.git'
set :branch, 'master'
set :server_name, 'snorraedda.is'
set :application, "snedda" # The name of the nginx config file
set :rbenv_path, "/usr/local/rbenv"

# Manually create these paths in shared/ (eg: shared/config/database.yml) in your server.
# They will be linked in the 'deploy:link_shared_paths' step.
set :shared_paths, ['config/database.yml', 'log', "public/audio", "public/crosswords"]
set :rails_env, ENV['RAILS_ENV'] || 'production'

set :forward_agent, true
# Optional settings:
#   set :user, 'foobar'    # Username in the server to SSH to.
#   set :port, '30000'     # SSH port number.

# This task is the environment that is loaded for most commands, such as
# `mina deploy` or `mina rake`.
task :environment do
  # If you're using rbenv, use this to load the rbenv environment.
  # Be sure to commit your .rbenv-version to your repository.
  # invoke :'rbenv:load'

  # For those using RVM, use this to load an RVM version@gemset.
  # invoke :'rvm:use[ruby-1.9.3-p125@default]'
end

desc "Create new folder structure + database.yml + vhost"
task :'setup' => :environment do
  queue! %[echo "-----> Setup folder structure on server"]
  invoke :'setup:directory'
  queue! %[echo "-----> Setup Nginx Virtualhost Configuration"]
  invoke :'setup:nginx'
  queue! %[echo "-----> Deploy Master for this version"]
  invoke :deploy
  queue! %[echo "-----> Link Virtualhost and restart Nginx"]
  invoke :'nginx:link'
  invoke :'nginx:reload'
end
# Put any custom mkdir's in here for when `mina setup` is ran.
# For Rails apps, we'll make some of the shared paths that are shared between
# all releases.
task :'setup:directory' => :environment do
  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]

  queue! %[touch "#{deploy_to}/shared/config/database.yml"]
  queue  %[echo "-----> Fill in information below to populate 'shared/config/database.yml'."]
  invoke :'setup:db:database_yml'
end
# Automatically populates the database.yml file
desc "Populate database.yml"
task :'setup:db:database_yml' => :environment do
  default = "sqlite3:///#{rails_env}.sqlite3" # The default database
  puts "Enter a URI for the new database (please make sure that the database exists). Default #{default}"
  db_str = STDIN.gets.chomp
  db = URI.parse(db_str.empty? ? default : db_str) # Parse the db string to more usable information
  # Create the YAML
  if db.scheme.start_with? "sqlite" # Sqlite activerecord settings are different
    database_yml = <<-DATABASE.dedent
      #{rails_env}:
        adapter: #{db.scheme == 'sqlite' ? 'sqlite3' : db.scheme}
        encoding: utf8
        database: #{deploy_to}/shared/db/#{db.path[1..-1]}
        pool: 5
        timeout: 5000
    DATABASE
    queue! %{
      mkdir -p "#{deploy_to}/shared/db"
      touch #{deploy_to}/shared/db/#{db.path[1..-1]}
    }

  else
    database_yml = <<-DATABASE.dedent
      #{rails_env}:
        adapter: #{db.scheme == 'postgres' ? 'postgresql' : db.scheme}
        encoding: utf8
        database: #{db.path[1..-1]}
        username: #{db.user}
        password: #{db.password}
        host: #{db.host}
        timeout: 5000
    DATABASE
  end
  # Output to server
  queue! %{
    echo "-----> Populating database.yml"
    echo "#{database_yml}" > #{deploy_to!}/shared/config/database.yml
    echo "-----> Done"
  }
end

# Create a new VirtualHost file
# Server name is defined by convention
# Script executes some sudo operations
desc "Create Nginx Virtualhost"
task :'setup:nginx' => :environment do
  # Virtualhost configuration file
  vhost = erb(File.expand_path('../../nginx.conf.erb', __FILE__)).gsub('"', '\\"').gsub('$', '\\$')
  queue! %{
    echo "-----> Create Nginx Virtualhost"
    echo "#{vhost}" > #{deploy_to!}/shared/config/nginx.conf
    echo "-----> Done"
  }
end




desc "Deploys the current version to the server."
task :deploy => :environment do
  deploy do
    # Put things that will set up an empty directory into a fully set-up
    # instance of your project.
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate'
    invoke :'rails:assets_precompile'

    to :launch do
      invoke :'website:restart'
    end
  end
end

# For help in making your deploy script, see the Mina documentation:
#
#  - http://nadarei.co/mina
#  - http://nadarei.co/mina/tasks
#  - http://nadarei.co/mina/settings
#  - http://nadarei.co/mina/helpers

namespace :nginx do
  nginx_config = "#{deploy_to!}/shared/config/nginx.conf"
  nginx_config_e = "/etc/nginx/sites-enabled/#{application}.conf"
  desc "Link nginx Virtualhost (enable site)"
  task :link do
    queue! %{
      echo "-----> Link Virtualhost file to /etc/nginx/sites-enabled/ (requires sudo)"
      sudo ln -nfs "#{nginx_config}" "#{nginx_config_e}"
    }
  end
  desc "Unlink nginx Virtualhost (disable site)"
  task :unlink do
    queue! %{
      echo "-----> Unlink Virtualhost file from /etc/nginx/sites-enabled/ (requires sudo)"
      sudo rm "#{nginx_config_e}"
    }
  end
  %w(stop start restart reload status).each do |action|
    desc "Nginx #{action.capitalize}"
    task action.to_sym => :environment do
      queue  %(echo "-----> #{action.capitalize} Nginx")
      queue! "sudo service nginx #{action}"
    end
  end
end

desc "Restarts the website server."
task :restart do
  invoke :'website:restart'
end

namespace :website do
  task :start do ; end
  task :stop do ; end
  task :restart do
    run "touch #{shared_path}/#{application}-restart.txt"
  end
end

namespace :service do
  %w(stop start restart status).each do |action|
    desc "Project #{action.capitalize}"
    task action.to_sym => :environment do
      queue  %(echo "-----> #{action.capitalize} Project service")
      queue! "sudo service #{application} #{action}"
    end
  end
end

class String
  def dedent
    lines = split "\n"
    return self if lines.empty?
    indents = lines.map do |line|
      line =~ /\S/ ? (line.start_with?(" ") ? line.match(/^ +/).offset(0)[1] : 0) : nil
    end
    min_indent = indents.compact.min
    return self if min_indent.zero?
    lines.map { |line| line =~ /\S/ ? line.gsub(/^ {#{min_indent}}/, "") : line }.join "\n"
  end
end
