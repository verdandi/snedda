Snedda::Application.routes.draw do

  devise_for :users, :skip => [:sessions, :registrations, :passwords]
  as :user do
    get 'login' => 'devise/sessions#new', :as => :new_user_session
    post 'login' => 'devise/sessions#create', :as => :user_session
    
    delete 'logout' => 'devise/sessions#destroy', :as => :destroy_user_session
    get 'logout' => 'devise/sessions#destroy'

    get 'register' => 'users/registrations#new', :as => :new_user_registration
    post 'register' => 'users/registrations#create', :as => :user_registration

    get 'user' => 'users/registrations#edit', :as => :edit_user_registration
    put 'register' => 'users/registrations#update'

    get 'reset' => 'devise/passwords#new', :as => :new_user_password
    get 'reset_password' => 'devise/passwords#edit', :as => :edit_user_password
    post 'reset' => 'devise/passwords#create', :as => :user_password
    put 'reset' => 'devise/passwords#update'
  end

  namespace :chapters do
    #Chapter group 1 routes
    get "chgroup1/chapters"

    #Chapter group 2 routes
    get "chgroup2/chapters"

    #Chapter group 3 routes
    get "chgroup4/chapters"

    #Chapter group 4 routes
    get "chgroup4/chapters"
    #Chapter group 5 routes
    get "chgroup5/chapters"

    #Chapter group 6 routes
    get "chgroup6/chapters"

    #Chapter group 7 routes
    get "chgroup7/chapters"

    #Chapter group 8 routes
    get "chgroup8/chapters"

    #Chapter group 8 routes
    get "chgroup9/chapters"

    get "chgroup10/chapters"
    get "chgroup11/chapters"
    get "chgroup12/chapters"
    get "chgroup13/chapters"

    get "map/map"
    get "exercises/index"
  end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'
  get '/index', to: 'home#index'
  get '/payment', to: 'payments#index', as: :new_payment
  post '/payment', to: 'payments#redirect'
  get '/payments/process', to: 'payments#purchase'
  post 'payments/process', to: 'payments#purchase'
  get '/payments/done', to: 'payments#done'
  get '/payment/:user', to: 'payments#redirect', as: :new_redirect_payment

  # Example of regular route:
  #get '/skopun' => 'chgroup1/group1chapters'

  get 'skopunheimsins', to: 'chapters/chgroup1#chapters'
  get 'heimsmyndin', to: 'chapters/chgroup2#chapters'
  get 'aesirogasynjur', to: 'chapters/chgroup3#chapters'
  get 'valholl', to: 'chapters/chgroup4#chapters'
  get 'sleipnirogskidbladnir', to: 'chapters/chgroup5#chapters'
  get 'forthorstilutgardaloka', to: 'chapters/chgroup6#chapters'
  get 'daudibaldurs', to: 'chapters/chgroup7#chapters'
  get 'ragnarok', to: 'chapters/chgroup8#chapters'


  get 'thjassiogidunn', to: 'chapters/chgroup9#chapters'
  get 'skaldamjodurinn', to: 'chapters/chgroup10#chapters'
  get 'forthors', to: 'chapters/chgroup11#chapters'
  get 'haddur', to: 'chapters/chgroup12#chapters'
  get 'fafnisarfur', to: 'chapters/chgroup13#chapters'

  get 'kort', to: 'chapters/map#map'
  get 'utgafa', to: 'chapters/utgafa#utgafa'

  get 'verkefni', to: 'chapters/exercises#index'
  scope '/verkefni', as: 'verkefni' do 
    get 'ritgerd(/:id)', to: 'chapters/exercises#essay', as: 'ritgerd'
    get 'ritun', to: 'chapters/exercises#writing', as: 'ritun'
    get 'krossar(/:id)', to: 'chapters/exercises#choice', as: 'krossar'
    post 'krossar/:id', to: 'chapters/exercises#choice_check'
    get 'tengi(/:id)', to: 'chapters/exercises#connect', as: 'tengi'
    post 'tengi/:id', to: 'chapters/exercises#connect_check'
    get 'malnotkun', to: 'chapters/exercises#language', as: 'malnotkun'
    get 'tulkun', to: 'chapters/exercises#interpretation', as: 'tulkun'
    get 'krossgatur', to: 'chapters/exercises#crossword', as: 'krossgatur'
  end

  get 'admin' => 'admin/users#index'
  get 'admin/users' => 'admin/users#index', :as => :admin_users
  get 'admin/users/admin/:id' => 'admin/users#admin_enable', :as => :admin_user_admin
  get 'admin/users/purchase/:id' => 'admin/users#admin_purchase', :as => :admin_user_purchase
  delete 'admin/users/purchase/:id' => 'admin/users#admin_clear_purchase'
  delete 'admin/users/admin/:id' => 'admin/users#admin_disable'
  

  # delete 'admin/users/logout' => 'admin/users#admin_logout', :as => :admin_user_logout
  delete 'admin/users/:id' => 'admin/users#delete', :as => :admin_user

  namespace :admin do 
    resources :quizzes do 
      resources :questions do 
        resources :answers
      end
    end
    resources :connect_quizzes do
      resources :connect_connections
    end
  end


  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
