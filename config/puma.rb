#!/usr/bin/env puma
 
threads 0, 4
# workers 3
 
bind  "unix:///var/tmp/puma/snedda.sock"
pidfile "/var/run/puma/snedda.pid"
environment "production"
stdout_redirect "/srv/puma/snedda.log"