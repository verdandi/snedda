class AddSaleIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :sale_id, :string
  end
end
