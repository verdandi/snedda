class CreateConnectQuizzes < ActiveRecord::Migration
  def change
    create_table :connect_quizzes do |t|
      t.string :name
      t.string :alias
      t.timestamps
    end
    add_index :connect_quizzes, :alias, unique: true
  end
end