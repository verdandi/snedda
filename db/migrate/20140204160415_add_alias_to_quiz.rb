class AddAliasToQuiz < ActiveRecord::Migration
  def change
    add_column :quizzes, :alias, :string
    add_index :quizzes, :alias, unique: true
  end
end
