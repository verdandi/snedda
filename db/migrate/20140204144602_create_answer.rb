class CreateAnswer < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :answer
      t.boolean :correct, default: false
      t.references :question, index: true
    end
  end
end
