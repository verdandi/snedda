class CreateConnectConnections < ActiveRecord::Migration
  def change
    create_table :connect_connections do |t|
      t.string :answer
      t.references :connect_quiz, index: true
      t.boolean :location
      t.integer :connect_id

      t.timestamps
    end
  end
end
