class CreateQuestionTable < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :question
      t.references :quiz, index: true
    end
  end
end
