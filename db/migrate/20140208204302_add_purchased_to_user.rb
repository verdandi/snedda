class AddPurchasedToUser < ActiveRecord::Migration
  def change
    add_column :users, :purchased, :boolean
  end
end
