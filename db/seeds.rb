# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

users = {
	"james@verdandi.is" => "jeska193sajd",
	"steindorein@verdandi.is" => "sfljasf@19",
	"throstur@verslo.is" => "sdofji!3jfsf",
	"gylfihaf@gmail.com" => "Jksf!jasf90",
	"verdandi@verdandi.is" => "123456789"
}
users.each do |email,password|
	if User.find_by_email(email).blank?
		puts "Creating user #{email} with password: #{password}"
		User.create(email: email.dup, password: password.dup, password_confirmation: password.dup, :admin => true, :purchased => true, :confirmation_token => email.dup + "BLEH", :confirmed_at => Time.now, :confirmation_sent_at => Time.now)
	end
end
