﻿/* TEAM */
	Company: Verðandi
	Contact: verdandi [at] verdandi.is
	From: Reykjavik, Iceland

	Designer: Þorbjörn Einar Guðmundsson
	Contact: thorbjorn [at] verdandi.is
	From: Reykjavik, Iceland

	Developer: James Elías Sigurðarson
	Contact: james [at] verdandi.is
	From: Reykjavik, Iceland

/* SITE */
	Last update:2015/01/19
	Language: Icelandic
	Doctype:HTML5